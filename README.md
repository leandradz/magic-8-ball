# Magic 8-Ball

## Visualizar resultado final:

Teste minha aplicação através do link: [https://leandradz.gitlab.io/magic-8-ball/](https://leandradz.gitlab.io/magic-8-ball/)

## Construa um Jogo de Azar

Implemente pelo menos um dos seguintes jogos usando HTML, CSS e JavaScript. Este será um projeto que você colocará em seu portfólio, então faça-o bonito e use a função do GitLab Pages para hospedar o jogo concluído para que qualquer um possa jogar.

Todos estes jogos irão precisar gerar números aleatórios, conforme a atividade de rolagem de dados desta sprint.

A grosso modo, aqueles que aparecem antes na lista são mais fáceis que os que aparecem depois (sujeito a variação de acordo com sua experiência).

Se estiver procurando desafios extras, você pode "aperfeiçoar" qualquer um desses jogos ao adicionar efeitos e animações de sobreposição de mouse.

## Magic 8-Ball
Concentre-se em uma pergunta de Sim / Não que você precisa muito da resposta e clique na Bola 8 Mágica para saber seu destino.
