const answers = [
  "É certo.",
  "É decididamente assim.",
  "Sem dúvida.",
  "Sim - definitivamente.",
  "Você pode confiar nisso.",
  "A meu ver, sim.",
  "Provavelmente.",
  "Sim.",
  "Os sinais apontam para sim.",
  "Resposta nebulosa, tente novamente.",
  "Pergunte novamente mais tarde.",
  "Melhor não te contar agora.",
  "Não é possível prever agora.",
  "Concentre-se e pergunte novamente.",
  "Não conte com isso.",
  "Minha resposta é não.",
  "Minhas fontes dizem não.",
  "A previsão não é tão boa.",
  "Muito duvidoso.",
]

function randomNumber() {
  let sorteio = Math.floor(Math.random() * 18 + 1)
  return sorteio
}

let button = document.getElementById('sendQuestion')
let div = document.getElementById('container')
let question = document.getElementById('question')
let bola8 = document.getElementById('bola8')
let number8 = document.getElementById('number8')

let span = document.createElement('span')
span.id = 'answers'
span.className = 'hidden'
bola8.appendChild(span)


button.onclick = function() {
  number8.removeAttribute('class')
  span.className = 'hidden'
  
  let sorteio = randomNumber()
  
  if (question.value == '') {
    bola8.removeAttribute('class')
    alert('Faça uma pergunta')
    return span.innerText = ''
  }
  
  for (let i = 0; i < answers.length; i++) {
    if (i === sorteio) {
      span.innerText = `${answers[i]}`
      number8.className = 'hidden'
      span.removeAttribute('class')
      bola8.className = 'answers'
    }
  }
  document.getElementById('question').value = ''
} 
